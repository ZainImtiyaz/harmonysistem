package main.project1.com.project1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import main.project1.com.model.*;
import main.project1.com.model.SearchCustomer;
import main.project1.com.util.ScheduleAdapter;
import main.project1.com.util.SearchCustomerAdapter;

/**
 * Created by CPU140252 on 20-Feb-17.
 */
@SuppressLint("ValidFragment")
public class SearchCustomerList extends Fragment {
    private String companyname;
    private TextView lblCompanyName;
    private ListView listSearchCustomer;
    private ArrayList<SearchCustomer> data;
    private SearchCustomerAdapter adapter;

    public SearchCustomerList(String companyname) {
        this.companyname = companyname;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.searchcustomerlist, container, false);
        lblCompanyName = (TextView) v.findViewById(R.id.lblCompanyName);
        listSearchCustomer = (ListView) v.findViewById(R.id.listSearchCustomer);

        lblCompanyName.setText(companyname);
        data = new ArrayList<SearchCustomer>();
        loadData();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        adapter = new SearchCustomerAdapter(getActivity(), data, ft, companyname);
        listSearchCustomer.setAdapter(adapter);
//        listSchedule.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.replace(R.id.content_frame, new ProfileCustomer()).addToBackStack(null);
//                ft.commit();
//            }
//        });
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Search Customer");
    }

    private void loadData() {
        SearchCustomer pm = new SearchCustomer();
        pm.setNo("1");
        pm.setUnit("Unit : MSN - 01");
        pm.setTime("12.00");
        pm.setJob("Job : PM");
        data.add(pm);
        pm = new SearchCustomer();
        pm.setNo("2");
        pm.setUnit("Unit : MSN - 01");
        pm.setTime("12.00");
        pm.setJob("Job : Cals");
        data.add(pm);
        pm = new SearchCustomer();
        pm.setNo("3");
        pm.setUnit("Unit : MSN - 01");
        pm.setTime("12.00");
        pm.setJob("Job : TC");
        data.add(pm);
    }
}

