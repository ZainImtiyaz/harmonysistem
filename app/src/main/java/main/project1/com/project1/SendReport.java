package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 06-Feb-17.
 */
public class SendReport extends Fragment {
    private TextView txtName;
    private Spinner spinArea, spinCompany, spinUnit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.sendreport, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        spinArea = (Spinner) v.findViewById(R.id.spinArea);
        spinCompany = (Spinner) v.findViewById(R.id.spinCompany);
        spinUnit = (Spinner) v.findViewById(R.id.spinUnit);

        txtName.setText(State.NAME);
        spinArea.setPrompt("Area");
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Report");
    }
}
