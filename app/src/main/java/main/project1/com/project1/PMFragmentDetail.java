package main.project1.com.project1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.model.PMDetail;
import main.project1.com.util.ProgressStatusUpdateInterface;
import main.project1.com.util.ScheduleAdapter;
import main.project1.com.util.State;

/**
 * Created by Yossi on 02-Feb-17.
 */
@SuppressLint("ValidFragment")
public class PMFragmentDetail extends Fragment {
    private String type;
    private TextView lblType;
    private ListView listSchedule;
    private ArrayList<PMDetail> data;
    private ScheduleAdapter adapter;

    public PMFragmentDetail(int statusnow, String type) {
        State.STATUS_PROSES_NOW = statusnow;
        this.type = type;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pmfragmentdetail, container, false);

        TeknikalDAO dao = new TeknikalDAO(getActivity(), new ProgressStatusUpdateInterface() {
            @Override
            public void onTaskDone(ArrayList<PMDetail> listTekn) {
                updateTask(listTekn);
            }
        }, State.PROGRESSSTATUSUPDATE);
        dao.execute();
        lblType = (TextView) v.findViewById(R.id.lblType);
        listSchedule = (ListView) v.findViewById(R.id.listSchedule);

        lblType.setText(type);
        data = new ArrayList<PMDetail>();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Jobs - " + State.STATUSJOB);
    }

    private void updateTask(ArrayList<PMDetail> listTekn) {
//        loadData();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        adapter = new ScheduleAdapter(getActivity(), listTekn, ft, type);
        listSchedule.setAdapter(adapter);
    }

//    public interface FragmentCallback {
//        public void onTaskDone();
//    }

    private void loadData() {
        PMDetail pm = new PMDetail();
        pm.setNo("1");
        pm.setPtName("PT. Inti Media Karna");
        pm.setTime("12.00");
        pm.setUnit("Unit : MSN - 01");
        pm.setLocation("Area : Bekasi");
        data.add(pm);
        pm = new PMDetail();
        pm.setNo("2");
        pm.setPtName("PT. Inti Media Karna");
        pm.setTime("01.00");
        pm.setUnit("Unit : MSN - 01");
        pm.setLocation("Area : Jakarta");
        data.add(pm);
        pm = new PMDetail();
        pm.setNo("3");
        pm.setNo("2");
        pm.setPtName("PT. Inti Media Karna");
        pm.setTime("02.00");
        pm.setUnit("Unit : MSN - 01");
        pm.setLocation("Area : Bandung");
        data.add(pm);
    }
}
