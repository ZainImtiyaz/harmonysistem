package main.project1.com.project1;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.util.State;

/**
 * Created by Yossi on 01-Feb-17.
 */
@SuppressLint("ValidFragment")
public class PMFragment extends Fragment implements View.OnClickListener {
    Button btnSchedule, btnNew, btnOnProcess, btnPending, btnClose;
    private TextView txtName, txtNotifSchedule, txtNotifNew, txtNotifOnProcess, txtNotifClose, txtNotifPending;
    private ImageView imgProfile;
    String type;

    public PMFragment(String type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.pmfragment, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        btnSchedule = (Button) v.findViewById(R.id.btnSchedule);
        btnNew = (Button) v.findViewById(R.id.btnNew);
        btnOnProcess = (Button) v.findViewById(R.id.btnOnProcess);
        btnClose = (Button) v.findViewById(R.id.btnClose);
        btnPending = (Button) v.findViewById(R.id.btnPending);
        imgProfile = (ImageView) v.findViewById(R.id.imgProfile);

        TeknikalDAO teknikalDAO = new TeknikalDAO(getActivity(), v, State.PROGRESSUPDATE);
        teknikalDAO.execute();
        txtName.setText(getString(R.string.hello) + ", " + State.NAME);
        btnSchedule.setOnClickListener(this);
        btnNew.setOnClickListener(this);
        btnOnProcess.setOnClickListener(this);
        btnClose.setOnClickListener(this);
        btnPending.setOnClickListener(this);

        if (type.equals(getString(R.string.callofservice)) || State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
            btnSchedule.setVisibility(View.INVISIBLE);
            RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) btnNew.getLayoutParams();
            param.addRule(RelativeLayout.BELOW, imgProfile.getId());
            btnNew.setLayoutParams(param);
        }
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Job");
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnSchedule:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    ft.replace(R.id.content_frame, new PMFragmentDetail(State.STATUS_PROSES_SCHEDULE,
                            getString(R.string.lblschedule))).addToBackStack(null);
                    ft.commit();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
//                    ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                    ft.replace(R.id.content_frame, new SearchCustomerList(getString(R.string.lblschedule))).addToBackStack(null);
                    ft.commit();
                }
                break;
            case R.id.btnNew:
//                ft.replace(R.id.content_frame, new StatusNowToOnProcess()).addToBackStack(null);
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    ft.replace(R.id.content_frame, new PMFragmentDetail(State.STATUS_PROSES_NEW,
                            getString(R.string.lblnew))).addToBackStack(null);
                    ft.commit();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
//                    ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                    ft.replace(R.id.content_frame, new SearchCustomerList(getString(R.string.lblnew))).addToBackStack(null);
                    ft.commit();
                }
                break;
            case R.id.btnOnProcess:
//                ft.replace(R.id.content_frame, new StatusOnProcessToClose()).addToBackStack(null);
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    ft.replace(R.id.content_frame, new PMFragmentDetail(State.STATUS_PROSES_ONPROSES,
                            getString(R.string.lblonprocess))).addToBackStack(null);
                    ft.commit();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
//                    ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                    ft.replace(R.id.content_frame, new SearchCustomerList(getString(R.string.lblonprocess))).addToBackStack(null);
                    ft.commit();
                }
                break;
            case R.id.btnClose:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    ft.replace(R.id.content_frame, new PMFragmentDetail(State.STATUS_PROSES_CLOSE,
                            getString(R.string.lblclose))).addToBackStack(null);
                    ft.commit();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
//                    ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                    ft.replace(R.id.content_frame, new SearchCustomerList(getString(R.string.lblclose))).addToBackStack(null);
                    ft.commit();
                }
                break;
            case R.id.btnPending:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    ft.replace(R.id.content_frame, new PMFragmentDetail(State.STATUS_PROSES_PENDING,
                            getString(R.string.lblpending))).addToBackStack(null);
                    ft.commit();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
//                    ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                    ft.replace(R.id.content_frame, new SearchCustomerList(getString(R.string.lblpending))).addToBackStack(null);
                    ft.commit();
                }
                break;
            default:
                break;
        }
    }
}

