package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Yossi on 07-Feb-17.
 */
public class StatusOnProcessToClose extends Fragment {
    private Spinner spinStatus, spinStatus1;
    private Button btnOK;
    private List<String> arrayStatus, arrayKeterangan;
    private static final int CLOSE = 4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.statusonprocesstoclose, container, false);
        spinStatus = (Spinner) v.findViewById(R.id.spinStatus);
        spinStatus1 = (Spinner) v.findViewById(R.id.spinStatus1);
        btnOK = (Button) v.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TeknikalDAO dao = new TeknikalDAO(getActivity(), v, State.ONPROCESS);
//                dao.execute();
                Log.e("tes", spinStatus.getSelectedItem().toString());
                Log.e("tes", spinStatus1.getSelectedItem().toString());
            }
        });

        arrayStatus = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.arraystatus)));
        arrayKeterangan = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.arraypic)));
        Log.e("status", arrayStatus.size() + "");
        for (int i = 0; i < arrayStatus.size(); i++) {
            if (arrayStatus.get(i).equals("Close")) {
                arrayStatus.remove(i);
            }
        }
        Log.e("status", arrayStatus.size() + "");
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayStatus);
        itemsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStatus.setAdapter(itemsAdapter);

        ArrayAdapter<String> itemsAdapter1 = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayKeterangan);
        itemsAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStatus1.setAdapter(itemsAdapter1);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile Customer");
    }
}
