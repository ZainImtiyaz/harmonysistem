package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.util.GetStatusNowInterface;
import main.project1.com.util.State;

/**
 * Created by Yossi on 07-Feb-17.
 */
public class StatusNowToOnProcess extends Fragment {
    private Spinner spinStatus, spinStatus1;
    private Button btnOK;
    private List<String> arrayStatus, arrayKeterangan;
    private TextView lblStatus;
    private static final int ONPROCESS = 3;
    private static final int PENDING = 5;
    private String id;

    public StatusNowToOnProcess(String id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.statusnowtoonprocess, container, false);

        lblStatus = (TextView) v.findViewById(R.id.lblStatus);
        spinStatus = (Spinner) v.findViewById(R.id.spinStatus);
        spinStatus1 = (Spinner) v.findViewById(R.id.spinStatus1);
        btnOK = (Button) v.findViewById(R.id.btnOK);
        TeknikalDAO dao = new TeknikalDAO(getActivity(), new GetStatusNowInterface() {
            @Override
            public void onTaskDone() {
                refresh();
            }
        }, State.GETSTATUSNOW, id);
        dao.execute();
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TeknikalDAO dao = new TeknikalDAO(getActivity(), v, State.ONPROCESS);
//                dao.execute();
                Log.e("tes", spinStatus.getSelectedItem().toString());
                Log.e("tes", spinStatus1.getSelectedItem().toString());
            }
        });
        return v;
    }

    private void refresh() {

        arrayStatus = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.arraystatus)));
        arrayKeterangan = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.arrayketerangan)));

        //-1 karena schedule dimulai dari 1
        String statusproses = State.STATUS_PROSES[State.STATUS_PROSES_NOW - 1];
        String statusprogress = "";
        if (State.STATUS_PROSES_NOW == State.STATUS_PROSES_ONPROSES) {
            statusprogress = State.STATUS_PROGRESS[State.STATUS_PROGRESS_NOW - 1];
            statusprogress = " (" + statusprogress + ") ";
        }
        lblStatus.setText(getString(R.string.statusnow) + " " + statusproses + statusprogress + " - " + State.STATUSJOB);
        //status onprocess dll
        for (int i = 0; i < arrayStatus.size(); i++) {
            if (arrayStatus.get(i).equals("Close")) {
                arrayStatus.remove(i);
            }
        }

        //keterangan visit dll
//        for (int i = 0; i < arrayKeterangan.size(); i++) {
//            //visit 1, inden 4, permintaan barang 5
//            if ((i + 1) == State.STATUS_PROGRESS_NOW) { //karena visit di database ==1
//                arrayKeterangan.remove(i);
//            }
//        }
        switch (State.STATUS_PROGRESS_NOW) {
            case State.STATUS_PROGRESS_VISIT:
                arrayKeterangan.remove(0);
                break;
            case State.STATUS_PROGRESS_INDEN:
                arrayKeterangan.remove(1);
                break;
            case State.STATUS_PROGRESS_PERMINTAANBARANG:
                arrayKeterangan.remove(2);
                break;
        }

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayStatus);
        itemsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStatus.setAdapter(itemsAdapter);

        ArrayAdapter<String> itemsAdapter1 = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_item, arrayKeterangan);
        itemsAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStatus1.setAdapter(itemsAdapter1);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile Customer");
    }
}
