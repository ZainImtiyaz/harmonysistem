package main.project1.com.project1;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Report extends Fragment implements View.OnClickListener {
    private TextView txtName;
    private Button btnSearchCustomer, btnReport, btnRequestTC;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mainreport, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        btnSearchCustomer = (Button) v.findViewById(R.id.btnSearchCustomer);
        btnReport = (Button) v.findViewById(R.id.btnReport);
        btnRequestTC = (Button) v.findViewById(R.id.btnRequestTC);

        txtName.setText(getString(R.string.hello) + ", " + State.NAME);
        btnSearchCustomer.setOnClickListener(this);
        btnReport.setOnClickListener(this);
        btnRequestTC.setOnClickListener(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Report");
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnSearchCustomer:
                ft.replace(R.id.content_frame, new SearchCustomer()).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnReport:
                ft.replace(R.id.content_frame, new SendReport()).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnRequestTC:
                ft.replace(R.id.content_frame, new RequestTC()).addToBackStack(null);
                ft.commit();
                break;
        }
    }
}
