package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class MainJob extends Fragment implements View.OnClickListener {
    Button btnPM, btnCallofService, btnTC, btnReschedule;
    private TextView txtName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mainjob, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        btnPM = (Button) v.findViewById(R.id.btnPM);
        btnCallofService = (Button) v.findViewById(R.id.btnCallofService);
        btnTC = (Button) v.findViewById(R.id.btnTC);
        btnReschedule = (Button) v.findViewById(R.id.btnReschedule);
        txtName.setText(getString(R.string.hello) + ", " + State.NAME);
        btnPM.setOnClickListener(this);
        btnCallofService.setOnClickListener(this);
        btnTC.setOnClickListener(this);
        btnReschedule.setOnClickListener(this);

        if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
            LinearLayout layout = (LinearLayout) v.findViewById(R.id.layoutMainJob);
            btnTC.setOnClickListener(null);
            layout.removeView(btnTC);
        }

        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Job");
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnPM:
                State.STATUSJOB = "PM";
                State.JENISPERINTAH_NOW = State.JENISPERINTAH_PM;
                ft.replace(R.id.content_frame, new PMFragment(getString(R.string.pm))).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnCallofService:
                State.STATUSJOB = "Cals";
                State.JENISPERINTAH_NOW = State.JENISPERINTAH_COS;
                ft.replace(R.id.content_frame, new PMFragment(getString(R.string.callofservice))).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnTC:
                State.STATUSJOB = "TC";
                State.JENISPERINTAH_NOW = State.JENISPERINTAH_TC;
                ft.replace(R.id.content_frame, new PMFragment(getString(R.string.tc))).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnReschedule:
                State.STATUSJOB = "Reschedule";
                State.JENISPERINTAH_NOW = State.JENISPERINTAH_RESCHEDULE;
                ft.replace(R.id.content_frame, new PMFragment(getString(R.string.reschedule))).addToBackStack(null);
                ft.commit();
                break;
        }
    }
}
