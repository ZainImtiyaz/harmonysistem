package main.project1.com.project1;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import main.project1.com.util.State;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private Bundle bundle;
    //    private String username;
    TextView txtName, txtEmail;
    android.support.v4.app.Fragment fragment = null;
    public static ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    public static DrawerLayout drawer;
    public static boolean isMainMenu = false;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //init
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

//        setContentView(R.layout.mainmenu);
//        ActionBar ab = getActionBar();
//        ab.setIcon(R.drawable.ic_mainmenu);
        //getActionBar().setTitle(Html.fromHtml("<font color='#000000'>" + "ActionBarTitle" + "</font>"));
        // Create a TextView programmatically.
//        TextView tv = new TextView(getApplicationContext());
//
//        // Create a LayoutParams for TextView
//        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
//                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
//                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
//
//        // Apply the layout parameters to TextView widget
//        tv.setLayoutParams(lp);
//        // Set text to display in TextView
//        // This will set the ActionBar title text
//        tv.setText("Android Example : ActionBar Title Style");
//
//        // Set the text color of TextView
//        // This will change the ActionBar title text color
//        tv.setTextColor(Color.parseColor("#FFF5EE"));
//
//        // Center align the ActionBar title
//        tv.setGravity(Gravity.CENTER);
//
//        // Set the serif font for TextView text
//        // This will change ActionBar title text font
//        tv.setTypeface(Typeface.SERIF, Typeface.ITALIC);
//
//        // Underline the ActionBar title text
//        tv.setPaintFlags(tv.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
//
//        // Set the ActionBar title font size
//        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP,12);
//
//        // Display a shadow around ActionBar title text
//        tv.setShadowLayer(
//                1.f, // radius
//                2.0f, // dx
//                2.0f, // dy
//                Color.parseColor("#FF8C00") // shadow color
//        );
//
//        // Set the ActionBar display option
//        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//
//        // Finally, set the newly created TextView as ActionBar custom view
//        ab.setCustomView(tv);

        Toolbar toolbar = (Toolbar) findViewById(
                R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        //skip
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() { //action onclick nav item
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        txtName = (TextView) headerView.findViewById(R.id.txtName);
        txtEmail = (TextView) headerView.findViewById(R.id.txtEmail);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            State.EMAIL = bundle.getString("email");
            State.KODEUSER = bundle.getString("kodeuser");
            // set name email and kode to sharedpref
            preferences = PreferenceManager.getDefaultSharedPreferences(this);
            editor.putString(getString(R.string.prefname), State.NAME);
            editor.putString(getString(R.string.prefemail), State.EMAIL);
            editor.putString(getString(R.string.prefkode), State.KODEUSER);
            editor.putString(getString(R.string.prefkateogriuser), State.KATEGORI_AKSES + "");
            editor.commit();

            txtName.setText(State.NAME);
            txtEmail.setText(State.EMAIL);
            fragment = new Home();
            if (fragment != null) {
                navigationView.setCheckedItem(R.id.nav_home);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, fragment);
                ft.commit();
            }
        }
    }

    public void changeFragment(Fragment fragment, boolean doAddToBackStack) {
//        toggle.setDrawerIndicatorEnabled(false); //icon action bar disable
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        if (doAddToBackStack) {
            transaction.addToBackStack(null);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_home);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toggle.syncState();
        }
        transaction.commit();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isMainMenu) {
            finish();
        }
        Fragment ft = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        ft = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        String fragmentNow = ft.getClass().getSimpleName();
        if (fragmentNow.equals("Home")) {
            toggle.setDrawerIndicatorEnabled(true); //hamburger icon action bar enable
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED); //slide open
            isMainMenu = true;
            navigationView.setCheckedItem(R.id.nav_home);
        }
//        if (isMainMenu) {
//            finish();
//        }
//        Fragment ft = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed(); //lakukan backpress dulu
//            ft = getSupportFragmentManager().findFragmentById(R.id.content_frame);
//            String fragmentNow = ft.getClass().getSimpleName();
//            Log.e("===", fragmentNow);
//            if (fragmentNow.equals("Home")) {
//                toggle.setDrawerIndicatorEnabled(true); //hamburger icon action bar enable
//                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                isMainMenu = true;
//                navigationView.setCheckedItem(R.id.nav_home);
//            } else if (fragmentNow.equals("Notification") || fragmentNow.equals("Profile")
//                    || fragmentNow.equals("Alert")) {
//                Log.e("TES", fragmentNow); //masih cek
////                navigationView.getMenu().performIdentifierAction(R.id.nav_home, 0);
//                FragmentTransaction ff = getSupportFragmentManager().beginTransaction();
//                ff.replace(R.id.content_frame, new Home());
//                ff.commit();
//                isMainMenu = true;
//            }
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
//       .

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        fragment = null;
        int id = item.getItemId();
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); //remove back state fragment sebelumnya
        isMainMenu = false;
        if (id == R.id.nav_home) {
            fragment = new Home();
            isMainMenu = true;
        } else if (id == R.id.nav_notification) {
            fragment = new Notification();
        } else if (id == R.id.nav_profile) {
            fragment = new Profile();
        } else if (id == R.id.nav_alert) {
            fragment = new Alert();
        } else if (id == R.id.logout) {
            //remove sharedpref
            editor = preferences.edit();
            editor.putString(getString(R.string.prefname), "");
            editor.putString(getString(R.string.prefemail), "");
            editor.putString(getString(R.string.prefkode), "");
            editor.commit();
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
            finish();
        }

        //replacing the fragment
        if (fragment != null) {
//            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
//            ft.replace(R.id.content_frame, fragment);
//            ft.commit();
            changeFragment(fragment, true);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
