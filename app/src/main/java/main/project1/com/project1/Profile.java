package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Profile extends Fragment {
    TextView txtKodePegawai, txtKodeArea, txtTTL, txtAlamat, txtPhone, txtTglDaftar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = null;
        if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
            v = inflater.inflate(R.layout.userprofile, container, false);
            TeknikalDAO dao = new TeknikalDAO(getActivity(), v, State.PROFILE);
            dao.execute();
        } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
            v = inflater.inflate(R.layout.userprofilecustomer, container, false);
            TeknikalDAO dao = new TeknikalDAO(getActivity(), v, State.PROFILE);
            dao.execute();
        }
        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile");
    }
}
