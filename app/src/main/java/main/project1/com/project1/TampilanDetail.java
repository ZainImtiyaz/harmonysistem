package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by CPU140252 on 10-Feb-17.
 */
public class TampilanDetail extends Fragment implements View.OnClickListener {
    private Button btnView;
    private String type;

    public TampilanDetail(String type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tampilan_detail_customer, container, false);
        btnView = (Button) v.findViewById(R.id.btnView);
        btnView.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnView:
                ft.replace(R.id.content_frame, new ViewProsesHistory(type)).addToBackStack(null);
                ft.commit();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile Customer");
    }
}

