package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.util.State;

/**
 * Created by Yossi on 18-Feb-17.
 */
public class ViewProsesHistory extends Fragment {

    private String unitpelanggan;

    public ViewProsesHistory(String unitpelanggan) {
        this.unitpelanggan = unitpelanggan;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.viewproseshistory, container, false);
        TeknikalDAO dao = new TeknikalDAO(getActivity(), v, State.PROFILEUNITPELANGGAN, unitpelanggan);
        dao.execute();
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Profile Customer");
    }
}
