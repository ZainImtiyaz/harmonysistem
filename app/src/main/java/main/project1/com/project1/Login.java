package main.project1.com.project1;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.util.Config;
import main.project1.com.util.GPSTracker;
import main.project1.com.util.NotificationUtils;
import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Login extends Activity {
    private TextView txtusername, txtpassword, txtforgotpassword;
    private Button btnlogin;
    private static String ADMIN = "admin";
    private static final String TAG = Login.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        //code here
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtusername = (TextView) findViewById(R.id.txtusername);
        txtpassword = (TextView) findViewById(R.id.txtpassword);
        btnlogin = (Button) findViewById(R.id.btnllogin);
        txtforgotpassword = (TextView) findViewById(R.id.txtforgotpassword);
        txtforgotpassword.setPaintFlags(txtforgotpassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        //reading sharedpref
        State.NAME = preferences.getString(getString(R.string.prefname), "");
        State.EMAIL = preferences.getString(getString(R.string.prefemail), "");
        State.KODEUSER = preferences.getString(getString(R.string.prefkode), "");

        // check sharedpref
        if (!State.NAME.equals("") && !State.EMAIL.equals("") && !State.KODEUSER.equals("")
                && !State.NAME.equals("")) {
            State.KATEGORI_AKSES = Integer.parseInt(preferences.getString(getString(R.string.prefkateogriuser), ""));
            Intent intent = new Intent(this,
                    MainActivity.class);
            intent.putExtra("email", State.EMAIL);
            intent.putExtra("kodeuser", State.KODEUSER);
            startActivity(intent);
            finish();
        }

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //register
//                Object object = null;
//                main.project1.com.model.Login lgn = new main.project1.com.model.Login();
//                lgn.setUsername(txtusername.getText().toString());
//                lgn.setPassword(txtpassword.getText().toString());
//                lgn.setIdfirebase(displayFirebaseRegId());
//                if (displayFirebaseRegId() != "") {
//                    object = lgn;
//                    TeknikalDAO dao = new TeknikalDAO(Login.this, object);
//                    dao.execute();
//                } else {
//                    Toast.makeText(Login.this, "Wait Id Firebase", Toast.LENGTH_LONG).show();
//                }
                //register

                //gps
//                gps = new GPSTracker(Login.this);
//
//                // check if GPS enabled
//                if (gps.canGetLocation()) {
//
//                    double latitude = gps.getLatitude();
//                    double longitude = gps.getLongitude();
//
//                    // \n is for new line
//                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
//                } else {
//                    // can't get location
//                    // GPS or Network is not enabled
//                    // Ask user to enable GPS/network in settings
//                    gps.showSettingsAlert();
//                }
                //end gps

                //login
//                Object object = null;
//                main.project1.com.model.Login lgn = new main.project1.com.model.Login();
//                lgn.setUsername("Yossi");
//                lgn.setPassword("1234");
//                object = lgn;
//                TeknikalDAO dao = new TeknikalDAO(Login.this, object);
//                dao.execute();
                //end login

                String username = txtusername.getText().toString();
                String password = txtpassword.getText().toString();
                Object object = null;
                main.project1.com.model.Login lgn = new main.project1.com.model.Login();
                lgn.setUsername(username);
                lgn.setPassword(password);
                object = lgn;
                TeknikalDAO dao = new TeknikalDAO(Login.this, object, State.LOGIN);
                dao.execute();
            }
        });

        txtforgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setBackgroundColor(Color.GRAY);
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        String idfirebase = "";
        //Firebast id selalu berubah ubah jika Share_Pref
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            //txtRegId.setText("Firebase Reg Id: " + regId);
//            Toast.makeText(this, "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            idfirebase = regId;
        } else {
//            Toast.makeText(this, "Firebase Reg Id is not received yet.", Toast.LENGTH_LONG).show();
        }
        return idfirebase;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
