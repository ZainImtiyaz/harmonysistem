package main.project1.com.project1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 06-Feb-17.
 */
public class SearchCustomer extends Fragment implements View.OnClickListener {
    private TextView txtName;
    private Button btnOK;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.searchcustomer, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        btnOK = (Button) v.findViewById(R.id.btnOK);

        txtName.setText(getString(R.string.hello) + ", " + State.NAME);
        btnOK.setOnClickListener(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Search Customer");
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnOK:
                ft.replace(R.id.content_frame, new SearchCustomerList("PT. Inti Media Karna")).addToBackStack(null);
                ft.commit();
                break;
        }
    }
}
