package main.project1.com.project1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Home extends Fragment implements View.OnClickListener {
    private Button btnUserProfile, btnJob, btnReport;
    private TextView txtName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = inflater.inflate(R.layout.home, container, false);
        txtName = (TextView) v.findViewById(R.id.txtName);
        btnUserProfile = (Button) v.findViewById(R.id.btnUserProfile);
        btnJob = (Button) v.findViewById(R.id.btnJobs);
        btnReport = (Button) v.findViewById(R.id.btnReport);
        btnUserProfile.setOnClickListener(this);
        btnJob.setOnClickListener(this);
        btnReport.setOnClickListener(this);

        txtName.setText(getString(R.string.hello) + ", " + State.NAME);
        btnUserProfile.setCompoundDrawables(getImageDrawable(R.drawable.user), null, null, null);
        btnJob.setCompoundDrawables(getImageDrawable(R.drawable.calendar), null, null, null);
        btnReport.setCompoundDrawables(getImageDrawable(R.drawable.speech_bubble), null, null, null);
        return v;
    }

    private Drawable getImageDrawable(int resource) { //resize image
        Drawable drawableUserProfile = getResources().getDrawable(resource);
        drawableUserProfile.setBounds(0, 0, (int) (drawableUserProfile.getIntrinsicWidth() * 0.5),
                (int) (drawableUserProfile.getIntrinsicHeight() * 0.5));
        ScaleDrawable sd = new ScaleDrawable(drawableUserProfile, 0, 50, 50);
        return sd.getDrawable();
    }

    @Override
    public void onClick(View view) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        MainActivity.isMainMenu = false;
        MainActivity.toggle.setDrawerIndicatorEnabled(false);
        MainActivity.drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);//slide close
        switch (view.getId()) {
            case R.id.btnUserProfile:
                ft.replace(R.id.content_frame, new Profile()).addToBackStack(null);
                ft.commit();
                NavigationView view1 = (NavigationView) getActivity().findViewById(R.id.nav_view);
                view1.setCheckedItem(R.id.nav_profile);
                break;
            case R.id.btnJobs:
                ft.replace(R.id.content_frame, new MainJob()).addToBackStack(null);
                ft.commit();
                break;
            case R.id.btnReport:
                ft.replace(R.id.content_frame, new Report()).addToBackStack(null);
                ft.commit();
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Home");
    }
}
