package main.project1.com.model;

/**
 * Created by CPU140252 on 20-Feb-17.
 */
public class SearchCustomer {
    private String no;
    private String unit;
    private String time;
    private String job;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
