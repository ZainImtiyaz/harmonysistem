package main.project1.com.model;

/**
 * Created by Yossi on 01-Mar-17.
 */
public class UnitPelanggan {
    private String kodeUnitPelanggan;
    private String namaArea;
    private String kodePelanggan;
    private String status;
    private String timeRilis;
    private String perihal;
    private String pic;
    private String informasi;

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }

    private String jenisLayanan;

    public String getKodeUnitPelanggan() {
        return kodeUnitPelanggan;
    }

    public void setKodeUnitPelanggan(String kodeUnitPelanggan) {
        this.kodeUnitPelanggan = kodeUnitPelanggan;
    }

    public String getNamaArea() {
        return namaArea;
    }

    public void setNamaArea(String namaArea) {
        this.namaArea = namaArea;
    }

    public String getKodePelanggan() {
        return kodePelanggan;
    }

    public void setKodePelanggan(String kodePelanggan) {
        this.kodePelanggan = kodePelanggan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimeRilis() {
        return timeRilis;
    }

    public void setTimeRilis(String timeRilis) {
        this.timeRilis = timeRilis;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getInformasi() {
        return informasi;
    }

    public void setInformasi(String informasi) {
        this.informasi = informasi;
    }
}
