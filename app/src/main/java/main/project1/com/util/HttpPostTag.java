package main.project1.com.util;

/**
 * Created by CPU140252 on 17-Feb-17.
 */
public class HttpPostTag {
    public static final String REQUEST = "request";
    public static final String USERNAME = "username";
    public static final String EMAIL = "email";
    public static final String ID = "id";
    public static final String PASSWORD = "password";
    public static final String KODETEKNIKAL = "kodeTeknikal";
    public static final String KODEPELANGGAN = "kodePelanggan";
    public static final String KODEJENISPERINTAH = "kodeJenisPerintah";
    public static final String KODEPROJECT = "kodeProject";
    public static final String KODEUNITPELANGGAN = "kodeUnitPelanggan";
    public static final String STATUS = "status";
    public static final String NAMAKOLOM = "namaKolom";
}
