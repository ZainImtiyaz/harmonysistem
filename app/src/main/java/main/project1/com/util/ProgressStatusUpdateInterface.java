package main.project1.com.util;

import java.util.ArrayList;

import main.project1.com.model.PMDetail;
import main.project1.com.model.Teknikal;

/**
 * Created by CPU140252 on 27-Feb-17.
 */
public interface ProgressStatusUpdateInterface {
    public void onTaskDone(ArrayList<PMDetail> listTekn);
}
