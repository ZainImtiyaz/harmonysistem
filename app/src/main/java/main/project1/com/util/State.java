package main.project1.com.util;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class State {
//    public static String KATEGORI_USER;
//    public static final String TEKNIKAL = "Teknikal";
//    public static final String CUSTOMER = "Pelanggan";

    public static int KATEGORI_AKSES = 0;
    public static final int USER_AKSES = 1;
    public static final int TEKNIKAL_AKSES = 2;
    public static final int PELANGGAN_AKSES = 3;

    public static String USERNAME;
    public static String EMAIL;
    public static String KODEUSER;
    public static String NAME;

    public static String KODEPROJECT;
    public static String STATUSJOB;
    public static String TYPEJOB;
    public static String KODEUNITPELANGGAN;

    public static String JENISPERINTAH_NOW;
    public static final String JENISPERINTAH_PM = "JPERINTAH-1";
    public static final String JENISPERINTAH_COS = "JPERINTAH-2";
    public static final String JENISPERINTAH_TC = "JPERINTAH-3";
    public static final String JENISPERINTAH_RESCHEDULE = "JPERINTAH-4";

    public static String[] STATUS_PROSES; //{"SCHEDULE", "NEW", "ON PROCESS", "CLOSE", "PENDING", "RESCHEDULE"};
    public static int STATUS_PROSES_NOW = 0;
    public static final int STATUS_PROSES_SCHEDULE = 1;
    public static final int STATUS_PROSES_NEW = 2;
    public static final int STATUS_PROSES_ONPROSES = 3;
    public static final int STATUS_PROSES_CLOSE = 4;
    public static final int STATUS_PROSES_PENDING = 5;
    public static final int STATUS_PROSES_RESCHEDULE = 6;

    public static String[] STATUS_PROGRESS; //= {"Visit", "Tanpa PIC", "Dengan PIC", "Inden", "Permintaan Barang", "Request TC"};
    public static int STATUS_PROGRESS_NOW = 0;
    public static final int STATUS_PROGRESS_VISIT = 1;
    public static final int STATUS_PROGRESS_TANPAPIC = 2;
    public static final int STATUS_PROGRESS_DENGANPIC = 3;
    public static final int STATUS_PROGRESS_INDEN = 4;
    public static final int STATUS_PROGRESS_PERMINTAANBARANG = 5;
    public static final int STATUS_PROGRESS_REQUESTTC = 6;

    public final static int LOGIN = 1;
    public final static int PROFILE = 2;
    public final static int PROGRESSUPDATE = 3;
    public final static int PROGRESSSTATUSUPDATE = 4;
    public final static int ONPROCESS = 5;
    public final static int PROFILEUNITPELANGGAN = 6;
    public final static int GETSTATUSNOW = 7;
    public final static int PROGRESSUPDATEPELANGGAN = 8;
    public final static int PROGRESSSTATUSUPDATEPELANGGAN = 9;
}
