package main.project1.com.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import main.project1.com.model.PMDetail;
import main.project1.com.model.SearchCustomer;
import main.project1.com.project1.R;
import main.project1.com.project1.StatusNowToOnProcess;
import main.project1.com.project1.StatusOnProcessToClose;
import main.project1.com.project1.StatusPending;
import main.project1.com.project1.TampilanDetail;
import main.project1.com.project1.ViewProsesHistory;

/**
 * Created by CPU140252 on 20-Feb-17.
 */
public class SearchCustomerAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<SearchCustomer> data;
    FragmentTransaction ft;
    String type;

    public SearchCustomerAdapter(Activity activity, ArrayList<SearchCustomer> data, FragmentTransaction ft, String type) {
        this.activity = activity;
        this.data = data;
        this.ft = ft;
        this.type = type;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
//        ft = getFragmentManager().beginTransaction();
        Log.e("TES", "position" + position);
        LayoutInflater inflater = (LayoutInflater)
                activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            vi = inflater.inflate(R.layout.itemsearchcustomer, null);

        TextView txtNo = (TextView) vi.findViewById(R.id.txtNo); // title
        TextView txtUnit = (TextView) vi.findViewById(R.id.txtUnit);
        TextView txtTime = (TextView) vi.findViewById(R.id.txtTime);
        TextView txtJob = (TextView) vi.findViewById(R.id.txtJob);

        Button btnView = (Button) vi.findViewById(R.id.btnView);
        btnView.setTag(position + "");
        Button btnDetail = (Button) vi.findViewById(R.id.btnDetail);
        btnDetail.setTag(position + "");

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ft.replace(R.id.content_frame, new ViewProsesHistory(type)).addToBackStack(null);
                ft.commit();
            }
        });
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ft.replace(R.id.content_frame, new TampilanDetail(v.getTag() + "")).addToBackStack(null);
                ft.commit();
            }
        });

        SearchCustomer detail = data.get(position);

        txtNo.setText(detail.getNo());
        txtUnit.setText(detail.getUnit());
        txtTime.setText(detail.getTime());
        txtJob.setText(detail.getJob());
        return vi;
    }
}
