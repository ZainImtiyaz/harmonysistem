package main.project1.com.util;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import main.project1.com.dao.TeknikalDAO;
import main.project1.com.model.PMDetail;
import main.project1.com.project1.ProfileCustomer;
import main.project1.com.project1.R;
import main.project1.com.project1.StatusNowToOnProcess;
import main.project1.com.project1.StatusOnProcessToClose;
import main.project1.com.project1.StatusPending;
import main.project1.com.project1.TampilanDetail;
import main.project1.com.project1.TampilanGet;
import main.project1.com.project1.ViewProsesHistory;

/**
 * Created by Yossi on 02-Feb-17.
 */
public class ScheduleAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<PMDetail> data;
    FragmentTransaction ft;
    String type;

    public ScheduleAdapter(Activity activity, ArrayList<PMDetail> data, FragmentTransaction ft, String type) {
        this.activity = activity;
        this.data = data;
        this.ft = ft;
        this.type = type;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
//        ft = getFragmentManager().beginTransaction();
        Log.e("TES", "position" + position);
        LayoutInflater inflater = (LayoutInflater)
                activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            vi = inflater.inflate(R.layout.itempmdetail, null);

        TextView txtNo = (TextView) vi.findViewById(R.id.txtNo); // title
        TextView txtPTName = (TextView) vi.findViewById(R.id.txtPTName);
        TextView txtUnit = (TextView) vi.findViewById(R.id.txtUnit);
        TextView txtTime = (TextView) vi.findViewById(R.id.txtTime);
        TextView txtLocation = (TextView) vi.findViewById(R.id.txtLocation);
        Log.e("TES", "Unit : " + txtUnit.getText());
        Button btnGet = (Button) vi.findViewById(R.id.btnGet);
        btnGet.setTag(position + "");
        Button btnView = (Button) vi.findViewById(R.id.btnView);
        btnView.setTag(position + "");
        Button btnDetail = (Button) vi.findViewById(R.id.btnDetail);
        btnDetail.setTag(position + "");
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = TeknikalDAO.pmdetail.get(Integer.parseInt(v.getTag() + "")).getId();
                Log.e("id", id);
                if (type.equalsIgnoreCase(activity.getString(R.string.lblschedule))) {
                    ft.replace(R.id.content_frame, new StatusNowToOnProcess(id)).addToBackStack(null);
                } else if (type.equalsIgnoreCase(activity.getString(R.string.lblnew))) {
                    ft.replace(R.id.content_frame, new StatusNowToOnProcess(id)).addToBackStack(null);
                } else if (type.equalsIgnoreCase(activity.getString(R.string.lblonprocess))) {
                    ft.replace(R.id.content_frame, new StatusNowToOnProcess(id)).addToBackStack(null);
                } else if (type.equalsIgnoreCase(activity.getString(R.string.lblpending))) {
                    ft.replace(R.id.content_frame, new StatusPending()).addToBackStack(null);
                } else if (type.equalsIgnoreCase(activity.getString(R.string.lblclose))) {
                    ft.replace(R.id.content_frame, new StatusOnProcessToClose()).addToBackStack(null);
                }
                ft.commit();
            }
        });
        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                State.KODEUNITPELANGGAN = TeknikalDAO.pmdetail.get(Integer.parseInt(v.getTag() + "")).getUnit();
                ft.replace(R.id.content_frame,
                        new ViewProsesHistory(State.KODEUNITPELANGGAN)).addToBackStack(null);
                ft.commit();
            }
        });
        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ft.replace(R.id.content_frame, new TampilanDetail(v.getTag() + "")).addToBackStack(null);
                ft.commit();
            }
        });
//        ImageView imgTime = (ImageView) vi.findViewById(R.id.imgTime);
//        ImageView imgLocation = (ImageView) vi.findViewById(R.id.imgLocation);
        PMDetail detail = data.get(position);

//        Log.e("sss", data.get(position).getNo() + " -- " + activity);

        txtNo.setText(detail.getNo());
        txtPTName.setText(detail.getPtName());
        txtTime.setText(detail.getTime());
        txtUnit.setText(detail.getUnit());
//        btnView.setTag(detail.getUnit() + "");
        txtLocation.setText(detail.getLocation());
        return vi;
    }
}
