package main.project1.com.util;

/**
 * Created by CPU140252 on 17-Feb-17.
 */
public class JSONTag {
    public static final String KATEGORIUSER = "kategoriuser";
    public static final String NAMA = "nama";
    public static final String ID = "id";
    public static final String KODE = "kode";
    public static final String KODE_UNIT = "kodeunit";
    public static final String KODEPROJECT ="kodeproject";
    public static final String EMAIL = "email";
    public static final String USER = "user";

    public static final String NAMA_PROJECT = "namaproject";
    public static final String STATUS_PROSES = "statusproses";
    public static final String STATUS_PROGRESS = "statusprogress";

    public static final String TIME = "time";
    public static final String KODE_AREA = "kode_area";
    public static final String TTL = "ttl";
    public static final String ALAMAT = "alamat";
    public static final String PHONE = "phone";
    public static final String TGL_GABUNG = "tgl_gabung";
    public static final String NAMA_AREA = "nama_area";
    public static final String JUMLAH_UNIT = "jumlah_unit";
    public static final String STATUS = "status";
    public static final String PIC = "pic";
    public static final String INFO = "info";
}
