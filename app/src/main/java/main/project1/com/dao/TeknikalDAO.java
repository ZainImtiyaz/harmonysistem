package main.project1.com.dao;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import main.project1.com.model.Customer;
import main.project1.com.model.Login;
import main.project1.com.model.PMDetail;
import main.project1.com.model.Teknikal;
import main.project1.com.model.UnitPelanggan;
import main.project1.com.project1.MainActivity;
import main.project1.com.project1.R;
import main.project1.com.util.ProgressStatusUpdateInterface;
import main.project1.com.util.Connection;
import main.project1.com.util.GetStatusNowInterface;
import main.project1.com.util.HTTPURLConnection;
import main.project1.com.util.HttpPostTag;
import main.project1.com.util.JSONTag;
import main.project1.com.util.ScheduleAdapter;
import main.project1.com.util.State;

/**
 * Created by CPU140252 on 20-Jan-17.
 */
public class TeknikalDAO extends AsyncTask<Void, Void, Boolean> {
    String response = "";
    //Create hashmap Object to send parameters to web service
    HashMap<String, String> postDataParams;
    private ProgressDialog pDialog;
    private Context context;
    private View v;
    private HTTPURLConnection service;
    private String path = "";
    private JSONObject json;
    private int success = 0;
    private boolean status = false;
    private Object obj;
    private int action = 0;
    private int Schedule = 0, New = 0, OnProcess = 0, Pending = 0, Close = 0, Reschedule;

    private String email, kodeuser, type;
    public static ArrayList<PMDetail> pmdetail;
    public static ArrayList<Teknikal> teknikal;
    public static ArrayList<Customer> customer;
    public static ArrayList<UnitPelanggan> unitpelanggan;
    private ScheduleAdapter adapter;

    private ProgressStatusUpdateInterface mProgressStatusUpdateInterface;
    private GetStatusNowInterface mGetStatusNowInterface;

    public TeknikalDAO(Context context, ProgressStatusUpdateInterface mProgressStatusUpdateInterface, int action) {
        this.context = context;
        this.mProgressStatusUpdateInterface = mProgressStatusUpdateInterface;
        this.action = action;
    }

    public TeknikalDAO(Context context, GetStatusNowInterface mGetStatusNowInterface, int action, String type) {
        this.context = context;
        this.mGetStatusNowInterface = mGetStatusNowInterface;
        this.action = action;
        this.type = type;
    }

    public TeknikalDAO(Context context, View v, int action) {
        this.context = context;
        this.action = action;
        this.v = v;
    }

    public TeknikalDAO(Context context, Object obj, int action) {
        this.context = context;
        this.action = action;
        this.obj = obj;
    }

    public TeknikalDAO(Context context, View v, int action, String type) {
        this.context = context;
        this.action = action;
        this.v = v;
        this.type = type;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        service = new HTTPURLConnection();
        path = context.getString(R.string.url_teknikaldao);

        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void typeAction() {
        switch (action) {
            case State.LOGIN://login
                Login lgn = (Login) obj;
                login(lgn);
                break;
            case State.PROFILE://profile
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    getProfileTeknikal(State.KODEUSER);
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                    getProfilePelanggan(State.KODEUSER);
                }
                break;
            case State.PROGRESSUPDATE:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    getAllProgressUpdate(State.KODEUSER, State.JENISPERINTAH_NOW);
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                    getAllProgressUpdate(State.KODEUSER, State.JENISPERINTAH_NOW);
                }
                break;
            case State.PROGRESSSTATUSUPDATE:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    getAllProgressUpdateStatus(State.KODEUSER, State.JENISPERINTAH_NOW, State.STATUS_PROSES_NOW);
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                    getAllProgressUpdateStatus(State.KODEUSER, State.JENISPERINTAH_NOW, State.STATUS_PROSES_NOW);
                }
                break;
            case State.ONPROCESS:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    insertStatusProses();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {

                }
                break;
            case State.PROFILEUNITPELANGGAN:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    getProfileUnitPelanggan();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {

                }
                break;
            case State.GETSTATUSNOW:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    getStatusProsesNow();
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {

                }
                break;
            default:
                break;
        }
    }

    private void updateUI() {
        switch (action) {
            case State.LOGIN://login
                if (status == true) {
                    Intent intent = new Intent(context,
                            MainActivity.class);
                    intent.putExtra("email", email);
                    intent.putExtra("kodeuser", kodeuser);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                } else {
                    Toast.makeText(context, "Username and Password not match", Toast.LENGTH_LONG).show();
                }
                break;
            case State.PROFILE://profile
                if (v != null) {
                    if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                        TextView txtKodePegawai = (TextView) v.findViewById(R.id.txtKodePegawai);
                        TextView txtNamaPegawai = (TextView) v.findViewById(R.id.txtNamaPegawai);
                        TextView txtKodeArea = (TextView) v.findViewById(R.id.txtKodeArea);
                        TextView txtTTL = (TextView) v.findViewById(R.id.txtTTL);
                        TextView txtAlamat = (TextView) v.findViewById(R.id.txtAlamat);
                        TextView txtPhone = (TextView) v.findViewById(R.id.txtPhone);
                        TextView txtTglDaftar = (TextView) v.findViewById(R.id.txtTglDaftar);
                        TextView txtArea = (TextView) v.findViewById(R.id.txtArea);
                        for (int i = 0; i < teknikal.size(); i++) {
                            txtKodePegawai.setText(teknikal.get(i).getKodePegawai());
                            txtNamaPegawai.setText(teknikal.get(i).getNama());
                            txtKodeArea.setText(teknikal.get(i).getKodeArea());
                            txtTTL.setText(teknikal.get(i).getTTL());
                            txtAlamat.setText(teknikal.get(i).getAlamat());
                            txtPhone.setText(teknikal.get(i).getPhone());
                            txtTglDaftar.setText(teknikal.get(i).getTglDaftar());
                            txtArea.setText(context.getString(R.string.lblarea) + teknikal.get(i).getNamaArea());
                        }
                    } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                        TextView txtKodePegawai = (TextView) v.findViewById(R.id.txtKodePegawai);
                        TextView txtNamaPegawai = (TextView) v.findViewById(R.id.txtNamaPegawai);
                        TextView txtKodeArea = (TextView) v.findViewById(R.id.txtKodeArea);
                        TextView txtTTL = (TextView) v.findViewById(R.id.txtTTL);
                        TextView txtAlamat = (TextView) v.findViewById(R.id.txtAlamat);
                        TextView txtPhone = (TextView) v.findViewById(R.id.txtPhone);
                        TextView txtTglDaftar = (TextView) v.findViewById(R.id.txtTglDaftar);
                        TextView txtArea = (TextView) v.findViewById(R.id.txtArea);
                        TextView txtUnit = (TextView) v.findViewById(R.id.txtUnit);
                        TextView txtPIC = (TextView) v.findViewById(R.id.txtPIC);

                        for (int i = 0; i < customer.size(); i++) {
                            txtKodePegawai.setText(customer.get(i).getKodePegawai());
                            txtNamaPegawai.setText(customer.get(i).getNama());
                            txtKodeArea.setText(customer.get(i).getKodeArea());
                            txtTTL.setText(customer.get(i).getTTL());
                            txtAlamat.setText(customer.get(i).getAlamat());
                            txtPhone.setText(customer.get(i).getPhone());
                            txtTglDaftar.setText(customer.get(i).getTglDaftar());
                            txtArea.setText(context.getString(R.string.lblarea) + customer.get(i).getNamaArea());
                            txtUnit.setText(customer.get(i).getJumlahUnit());
                            txtPIC.setText(customer.get(i).getPIC());
                        }
                    }
                }
                break;
            case State.PROGRESSUPDATE:
                TextView txtNotifSchedule = (TextView) v.findViewById(R.id.txtNotifSchedule);
                txtNotifSchedule.setText(Schedule + "");
                TextView txtNotifNew = (TextView) v.findViewById(R.id.txtNotifNew);
                txtNotifNew.setText(New + "");
                TextView txtNotifOnProcess = (TextView) v.findViewById(R.id.txtNotifOnProcess);
                txtNotifOnProcess.setText(OnProcess + "");
                TextView txtNotifClose = (TextView) v.findViewById(R.id.txtNotifClose);
                txtNotifClose.setText(Close + "");
                TextView txtNotifPending = (TextView) v.findViewById(R.id.txtNotifPending);
                txtNotifPending.setText(Pending + "");
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    if (State.JENISPERINTAH_NOW.equals(State.JENISPERINTAH_COS) ||
                            State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                        txtNotifSchedule.setVisibility(View.INVISIBLE);
                    }
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
                    txtNotifSchedule.setVisibility(View.INVISIBLE);
                }
                break;
            case State.PROGRESSSTATUSUPDATE:
                mProgressStatusUpdateInterface.onTaskDone(pmdetail);
                break;
            case State.ONPROCESS:
                break;
            case State.PROFILEUNITPELANGGAN:
                if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
                    TextView txtPTName = (TextView) v.findViewById(R.id.txtPTName);
                    TextView txtLocation = (TextView) v.findViewById(R.id.txtLocation);
                    TextView txtKodePelanggan = (TextView) v.findViewById(R.id.txtKodePelanggan);
                    TextView txtStatus = (TextView) v.findViewById(R.id.txtStatus);
                    TextView txtTglProses = (TextView) v.findViewById(R.id.txtTglProses);
                    TextView txtJenisLayanan = (TextView) v.findViewById(R.id.txtJenisLayanan);
                    TextView txtPIC = (TextView) v.findViewById(R.id.txtPIC);
                    TextView txtPerihal = (TextView) v.findViewById(R.id.txtPerihal);
                    TextView txtInformasi = (TextView) v.findViewById(R.id.txtInformasi);
                    for (int i = 0; i < unitpelanggan.size(); i++) {
                        txtPTName.setText(unitpelanggan.get(i).getKodeUnitPelanggan());
                        txtLocation.setText(unitpelanggan.get(i).getNamaArea());
                        txtKodePelanggan.setText(unitpelanggan.get(i).getKodePelanggan());
                        txtStatus.setText(unitpelanggan.get(i).getStatus());
                        txtTglProses.setText(unitpelanggan.get(i).getTimeRilis());
                        txtJenisLayanan.setText(unitpelanggan.get(i).getJenisLayanan());
                        txtPIC.setText(unitpelanggan.get(i).getPic());
                        txtPerihal.setText(unitpelanggan.get(i).getPerihal());
                        txtInformasi.setText(unitpelanggan.get(i).getInformasi());
                    }
                } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {

                }
                break;
            case State.GETSTATUSNOW:
                mGetStatusNowInterface.onTaskDone();
                break;
            default:
                break;
        }

    }

    @Override
    protected Boolean doInBackground(Void... arg0) {
        Connection connection = new Connection(context);
        boolean status = connection.isActivateInternetConnection();
        if (State.STATUS_PROSES == null) {
            getStatusProsesName();
        }
        if (State.STATUS_PROGRESS == null) {
            getStatusProgressName();
        }
        typeAction();
        return status;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (pDialog.isShowing())
            pDialog.dismiss();
        if (result) {
            updateUI();
        } else {
            Toast.makeText(context, context.getString(R.string.nointernet), Toast.LENGTH_LONG).show();
        }
    }

    private boolean login(Login lgn) { //fungsi login diubah true? karena harusnya user dan pass bukan email dan pass...
        service = new HTTPURLConnection();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestlogin));
        postDataParams.put(HttpPostTag.USERNAME, lgn.getUsername());
        postDataParams.put(HttpPostTag.PASSWORD, lgn.getPassword());
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
//            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
//            json = new JSONObject(response);
//            email = json.getString(JSONTag.EMAIL);
            JSONArray login = new JSONArray(response);
            for (int i = 0; i < login.length(); i++) {
                JSONObject jsonObject = login.getJSONObject(i);
                email = jsonObject.getString(JSONTag.EMAIL);
                State.KATEGORI_AKSES = jsonObject.getInt(JSONTag.USER);
                kodeuser = lgn.getUsername();
                status = true;
                State.NAME = getNama(kodeuser);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
        return status;
    }

    private void getProfileTeknikal(String kode_teknikal) { //ambil profile teknikal
        service = new HTTPURLConnection();
        teknikal = new ArrayList<Teknikal>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestprofileteknikal));
        postDataParams.put(HttpPostTag.KODETEKNIKAL, kode_teknikal);
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);
            Log.e("tes", profile.length() + "");
            for (int i = 0; i < profile.length(); i++) {
                Teknikal tkn = new Teknikal();
                JSONObject jsonObject = profile.getJSONObject(i);
                tkn.setKodePegawai(": " + jsonObject.getString(JSONTag.KODE));
                tkn.setNama(jsonObject.getString(JSONTag.NAMA));
                tkn.setKodeArea(": " + jsonObject.getString(JSONTag.KODE_AREA));
                tkn.setTTL(": " + jsonObject.getString(JSONTag.TTL));
                tkn.setAlamat(": " + jsonObject.getString(JSONTag.ALAMAT));
                tkn.setPhone(": " + jsonObject.getString(JSONTag.PHONE));
                tkn.setTglDaftar(": " + jsonObject.getString(JSONTag.TGL_GABUNG));
                tkn.setNamaArea(jsonObject.getString(JSONTag.NAMA_AREA));
                teknikal.add(tkn);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getProfilePelanggan(String kode_pelanggan) { //ambil profile teknikal
        service = new HTTPURLConnection();
        customer = new ArrayList<Customer>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestprofilepelanggan));
        postDataParams.put(HttpPostTag.KODEPELANGGAN, kode_pelanggan);
        response = service.ServerData(path, postDataParams);
        Log.e("response pelanggan", response);
        try {
            JSONArray profile = new JSONArray(response);
            for (int i = 0; i < profile.length(); i++) {
                Customer tkn = new Customer();
                JSONObject jsonObject = profile.getJSONObject(i);
                tkn.setKodePegawai(": " + jsonObject.getString(JSONTag.KODE));
                tkn.setNama(jsonObject.getString(JSONTag.NAMA));
                tkn.setKodeArea(": " + jsonObject.getString(JSONTag.KODE_AREA));
                tkn.setTTL(": " + jsonObject.getString(JSONTag.TTL));
                tkn.setAlamat(": " + jsonObject.getString(JSONTag.ALAMAT));
                tkn.setPhone(": " + jsonObject.getString(JSONTag.PHONE));
                tkn.setTglDaftar(": " + jsonObject.getString(JSONTag.TGL_GABUNG));
                tkn.setNamaArea(jsonObject.getString(JSONTag.NAMA_AREA));
                tkn.setJumlahUnit(": " + jsonObject.getString(JSONTag.JUMLAH_UNIT));
                tkn.setPIC(": " + jsonObject.getString(JSONTag.PIC));
                customer.add(tkn);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getStatusProsesName() {
        service = new HTTPURLConnection();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetstatusprosesname));
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray obj = new JSONArray(response);
            State.STATUS_PROSES = new String[obj.length()];
            for (int i = 0; i < obj.length(); i++) {
                JSONObject jsonObject = obj.getJSONObject(i);
                State.STATUS_PROSES[i] = jsonObject.getString(JSONTag.STATUS_PROSES);
            }
            for (int i = 0; i < State.STATUS_PROSES.length; i++) {
                Log.e("Proses ", State.STATUS_PROSES[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getStatusProgressName() {
        service = new HTTPURLConnection();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetstatusprogressname));
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray obj = new JSONArray(response);
            State.STATUS_PROGRESS = new String[obj.length()];
            for (int i = 0; i < obj.length(); i++) {
                JSONObject jsonObject = obj.getJSONObject(i);
                State.STATUS_PROGRESS[i] = jsonObject.getString(JSONTag.STATUS_PROGRESS);
            }
            for (int i = 0; i < State.STATUS_PROGRESS.length; i++) {
                Log.e("Progress ", State.STATUS_PROGRESS[i]);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getAllProgressUpdate(String kode_teknikal, String kode_jenisperintah) { //ambil jumlah data
        service = new HTTPURLConnection();
        teknikal = new ArrayList<Teknikal>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetallprogressupdate));
        postDataParams.put(HttpPostTag.KODETEKNIKAL, kode_teknikal);
        postDataParams.put(HttpPostTag.KODEJENISPERINTAH, kode_jenisperintah);
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);
            for (int i = 0; i < profile.length(); i++) {
                Teknikal tkn = new Teknikal();
                JSONObject jsonObject = profile.getJSONObject(i);
                tkn.setId(": " + jsonObject.getString(JSONTag.ID));
                tkn.setNamaProject(jsonObject.getString(JSONTag.NAMA_PROJECT));
                int statusproses = jsonObject.getInt(JSONTag.STATUS_PROSES);
                tkn.setStatusProses(": " + statusproses);
//                String statusprocess = tkn.getStatusProses();
//                statusprocess = statusprocess.substring(2, statusprocess.length());
//                Log.e(statusprocess, statusprocess.length() + "");

                if (statusproses == State.STATUS_PROSES_SCHEDULE) {
                    Log.e("close", "sch");
                    Schedule++;
                } else if (statusproses == State.STATUS_PROSES_NEW) {
                    Log.e("new", "yes");
                    New++;
                } else if (statusproses == State.STATUS_PROSES_ONPROSES) {
                    Log.e("on proc", "yes");
                    OnProcess++;
                } else if (statusproses == State.STATUS_PROSES_PENDING) {
                    Log.e("pending", "yes");
                    Pending++;
                } else if (statusproses == State.STATUS_PROSES_CLOSE) {
                    Log.e("close", "yes");
                    Close++;
                } else if (statusproses == State.STATUS_PROSES_RESCHEDULE) {
                    Log.e("close", "yes");
                    Reschedule++;
                }
                teknikal.add(tkn);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getAllProgressUpdateStatus(String kode_teknikal, String kode_jenisperintah, int status) { //ambil list data job desk
        service = new HTTPURLConnection();
        pmdetail = new ArrayList<PMDetail>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetallprogressupdatestatus));
        postDataParams.put(HttpPostTag.KODETEKNIKAL, kode_teknikal);
        postDataParams.put(HttpPostTag.KODEJENISPERINTAH, kode_jenisperintah);
        postDataParams.put(HttpPostTag.STATUS, status + "");
        String namakolom = "";
        if (status == State.STATUS_PROSES_SCHEDULE) {
            namakolom = "time_start";
        } else if (status == State.STATUS_PROSES_NEW) {
            namakolom = "time_start";
        } else if (status == State.STATUS_PROSES_ONPROSES
                || status == State.STATUS_PROSES_PENDING) {
            namakolom = "time_progress";
        } else if (status == State.STATUS_PROSES_CLOSE) {
            namakolom = "time_close";
        }
        Log.e("...", kode_teknikal + " || " + kode_jenisperintah + " || " + status + " || " + namakolom);
        postDataParams.put(HttpPostTag.NAMAKOLOM, namakolom);
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);
            for (int i = 0; i < profile.length(); i++) {
                PMDetail pm = new PMDetail();
                JSONObject jsonObject = profile.getJSONObject(i);
                pm.setNo((i + 1) + "");
                pm.setId(jsonObject.getString(JSONTag.ID));
                pm.setPtName(jsonObject.getString(JSONTag.KODE));
                State.KODEPROJECT = jsonObject.getString(JSONTag.KODEPROJECT);
                Log.e("kode Project", State.KODEPROJECT);
                pm.setTime(jsonObject.getString(JSONTag.TIME));
                pm.setUnit(jsonObject.getString(JSONTag.KODE_UNIT));
                pm.setLocation(jsonObject.getString(JSONTag.NAMA_AREA));
                State.KODEUNITPELANGGAN = pm.getUnit();
                pmdetail.add(pm);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getProfileUnitPelanggan() { //ambil profile unit pelanggan
        service = new HTTPURLConnection();
        unitpelanggan = new ArrayList<UnitPelanggan>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetprofileunitpelanggan));
        postDataParams.put(HttpPostTag.KODEUNITPELANGGAN, type);

        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);
            for (int i = 0; i < profile.length(); i++) {
                UnitPelanggan up = new UnitPelanggan();
                JSONObject jsonObject = profile.getJSONObject(i);
                up.setKodeUnitPelanggan(context.getString(R.string.unit) + " : " + State.KODEUNITPELANGGAN);
                up.setNamaArea(context.getString(R.string.lblarea) + " " + jsonObject.getString(JSONTag.NAMA_AREA));
                up.setKodePelanggan(": " + jsonObject.getString(JSONTag.KODE));
                up.setTimeRilis(": " + jsonObject.getString(JSONTag.TIME));
                up.setJenisLayanan(": " + State.STATUSJOB);
                up.setStatus(": " + jsonObject.getString(JSONTag.STATUS));
                up.setPerihal(": " + "-");
                up.setPic(": " + jsonObject.getString(JSONTag.PIC));
                up.setInformasi(": " + jsonObject.getString(JSONTag.INFO));
                unitpelanggan.add(up);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void getStatusProsesNow() {
        service = new HTTPURLConnection();
        unitpelanggan = new ArrayList<UnitPelanggan>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestgetstatusprosesnow));
        postDataParams.put(HttpPostTag.ID, type);

        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);
            for (int i = 0; i < profile.length(); i++) {
                JSONObject jsonObject = profile.getJSONObject(i);
                State.STATUS_PROSES_NOW = jsonObject.getInt(JSONTag.STATUS_PROSES);
                State.STATUS_PROGRESS_NOW = jsonObject.getInt(JSONTag.STATUS_PROGRESS);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private void insertStatusProses() { //insert data ke progress project
        service = new HTTPURLConnection();
        unitpelanggan = new ArrayList<UnitPelanggan>();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestinsertstatusproses));
        postDataParams.put(HttpPostTag.KODETEKNIKAL, State.KODEUSER);
        postDataParams.put(HttpPostTag.KODEJENISPERINTAH, State.JENISPERINTAH_NOW);
        postDataParams.put(HttpPostTag.STATUS, State.TYPEJOB);
        postDataParams.put(HttpPostTag.KODEPROJECT, State.KODEPROJECT);

        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            JSONArray profile = new JSONArray(response);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }

    private String getNama(String kode) {
        service = new HTTPURLConnection(); //init
        postDataParams = new HashMap<String, String>();
        if (State.KATEGORI_AKSES == State.TEKNIKAL_AKSES) {
            Log.e("nama?", "teknikal");
            postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestnamateknikal));
            postDataParams.put(HttpPostTag.KODETEKNIKAL, kode);
        } else if (State.KATEGORI_AKSES == State.PELANGGAN_AKSES) {
            Log.e("nama?", "pelanggan");
            postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestnamapelanggan));
            postDataParams.put(HttpPostTag.KODEPELANGGAN, kode);
        } else {
            Log.e("nama?", State.KATEGORI_AKSES + "");
        }
        response = service.ServerData(path, postDataParams);
        Log.e("nama?", response);
        try {
            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
            json = new JSONObject(response);
            String nama = json.getString(JSONTag.NAMA);
            return nama;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
        return null;
    }

    private String getCountJob(String kodeTeknikal, String kodeProject) {
        service = new HTTPURLConnection();
        postDataParams = new HashMap<String, String>();
        postDataParams.put(HttpPostTag.REQUEST, context.getString(R.string.requestcountjob));
        postDataParams.put(HttpPostTag.KODETEKNIKAL, kodeTeknikal);
        postDataParams.put(HttpPostTag.KODEPROJECT, kodeProject);
        response = service.ServerData(path, postDataParams);
        Log.e("nama?", response);
        try {
            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
            json = new JSONObject(response);
            String nama = json.getString(JSONTag.NAMA);
            Log.e("nama?11", nama);
            return nama;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
        return null;
    }

    private void insert() {
        service = new HTTPURLConnection();
        path = "http://yosisetiawan.esy.es/insert%20database/addemployee.php";
        postDataParams = new HashMap<String, String>();
//        postDataParams.put("username", username);
//        postDataParams.put("password", password);
//        postDataParams.put("idfirebase", idfirebase);
        //postDataParams.put("address", strAddress);
        //Call ServerData() method to call webservice and store result in response
        response = service.ServerData(path, postDataParams);
        try {
            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
            json = new JSONObject(response);
            success = json.getInt("success");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }
}